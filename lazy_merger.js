define([
	'require',
	'madjoh_modules/cache/data_cache',
	'madjoh_modules/session/session'
], 
function(require, DataCache, Session){

	LazyMerger = {
		get : function(startIndex, cacheKey, getList, options){
			if(!options) 			options = {};
			if(!options.page_index) options.page_index = 'page_index';
			if(!options.merger) 	options.merger = LazyMerger.mergeData;
			if(options.concatData)	options.merger = LazyMerger.concatData;

			var cachedData = DataCache.get(cacheKey, options.delay);

			if(cachedData && !startIndex){
				return {
					async 	: false,
					promise : Promise.resolve(cachedData)
				};
			}else{
				if(!cachedData) startIndex = null;

				var promise = Promise.resolve([]);
				if(Session.isOpen()){
					var promise = getList(startIndex, options).then(function(elements){
						if(elements.length === 0 && startIndex) return null;

						cachedData = options.merger(elements, cachedData, options);
						DataCache.set(cacheKey, cachedData, options.noStorage);
						return cachedData;
					}, options);
				}

				return {
					async 	: true,
					promise : promise
				};
			}
		},
		mergeData : function(elements, cachedData, options){
			cachedData = cachedData ? cachedData : [];
			for(var i = elements.length - 1; i >= 0; i--){
				var found = false;
				for(var j = 0; j < cachedData.length; j++){
					if(cachedData[j][options.page_index] === elements[i][options.page_index]){
						cachedData[j] = elements[i];
						found = true;
						break;
					}
					if(options.uniqueKey && cachedData[j][options.uniqueKey] === elements[i][options.uniqueKey]){
						cachedData[j] = elements[i];
						found = true;
						break;
					}
				}
				if(found) elements.splice(i, 1);
			}
			cachedData = cachedData.concat(elements);
			return cachedData;
		},
		concatData : function(elements, cachedData, options){
			if(!cachedData) cachedData = [];
			return cachedData.concat(data);
		}
	};

	return LazyMerger;
});