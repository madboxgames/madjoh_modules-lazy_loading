define(function(){
	var ArrayTools = {
		search : function(array, key, targetValue, start, end, order){
			if(!start) 	start = 0;
			if(!end) 	end = array.length - 1;
			if(!order) 	order = (array[start][key] > array[end][key]) ? -1 : 1; // CROISSANT OU DECROISSANT

			if(array[start][key] 	=== targetValue) return start;
			if(array[end][key] 		=== targetValue) return end;
			if(start === end && array[start][key] !== targetValue) return -1;

			var middle = Math.floor((start + end) / 2);
			var middleValue = array[middle][key];

				 if((order === 1 && middleValue > targetValue) || (order === -1 && middleValue < targetValue)) return ArrayTools.search(array, key, targetValue, start + 1, middle, order);
			else if((order === 1 && middleValue < targetValue) || (order === -1 && middleValue > targetValue)) return ArrayTools.search(array, key, targetValue, middle, 	end - 1, order);

			return middle;
		}
	};

	return ArrayTools;
});
