define([
	'require',
	'madjoh_modules/styling/styling',
	'madjoh_modules/controls/controls'
],
function(require, Styling, Controls){
	function LazyLoader(page, elementClass, options){
		if(!options) options = {};

		this.page 				= page;
		this.scroller 			= page.scroller;
		this.list 				= page.list;
		this.elementClass 		= elementClass;
		this.page_index 		= options.page_index 		|| 'page_index';
		this.screenEndRatio 	= options.screenEndRatio 	|| 1.5;
		this.order 				= options.order 			|| -1;
		this.scrollDirection	= options.scrollDirection 	|| 'vertical';

		// LISTENERS
			var lazyLoader = this;
			page.scroller.addEventListener('scroll', function(){lazyLoader.checkScroll();}, false);

		if(this.page.errorContainer){
			var reloadIcon = this.page.errorContainer.querySelector('.error-message-reload');
			if(!reloadIcon) return console.log('Malformed error container on page ', page);
			reloadIcon.addEventListener(Controls.click, function(){
				if(lazyLoader.startIndex){
					lazyLoader.startIndex = null;
					lazyLoader.checkScroll();
				}else{
					lazyLoader.getList();
				}
			}, false);
		}
	}

	// GET LIST
		LazyLoader.prototype.getList = function(startIndex, resetting){
			this.loading = true;

			if(this.page.placeholder) 		Styling.addClass(this.page.placeholder, 	'loading');
			if(this.page.errorContainer) 	Styling.addClass(this.page.errorContainer, 	'hidden');
			if(this.page.upToDateMessage) 	Styling.addClass(this.page.upToDateMessage, 'hidden');

			this.lockSearch();

			var result = this.page.getList(startIndex);

			if(result.async && !resetting){
				if(this.page.loadingWheel) this.page.loadingWheel.show();
			}

			var lazyLoader = this;
			var promise = result.promise.then(function(elements){return lazyLoader.showList(elements);}, function(error){
				if(error.printStackTrace) error.printStackTrace();
				else{
					var exception = new Error(error);
					console.log(error, exception.stack);
				}

				lazyLoader.loading = false;
				lazyLoader.unlockSearch();

				if(lazyLoader.page.errorContainer) 	Styling.removeClass(lazyLoader.page.errorContainer, 'hidden');
				if(lazyLoader.page.placeholder) 	Styling.removeClass(lazyLoader.page.placeholder, 	'active');
				if(lazyLoader.page.loadingWheel) 	lazyLoader.page.loadingWheel.hide();
				if(lazyLoader.page.loader) 			lazyLoader.page.loader.desactivate();
			}).catch(function(showListError){
				console.log(showListError);
				var error = new Error(showListError);
				console.log(error.stack);
			});

			return promise;
		};
		LazyLoader.prototype.showList = function(elements){
			this.loading = false;

			if(elements) this.page.showList(elements);

			if(this.page.mediaLoader) this.page.mediaLoader.loadImages();

			this.unlockSearch();
			if(this.page.onSearch) this.page.onSearch();

			var displayedElements = this.list.querySelectorAll('.' + this.elementClass);
			if(displayedElements.length > 0) 	Styling.removeClass(this.page.scroller, 'empty');
			else 								Styling.addClass(this.page.scroller, 	'empty');

			if(this.page.loadingWheel) 	this.page.loadingWheel.hide();
			if(this.page.loader) 		this.page.loader.desactivate();

			if(this.page.upToDateMessage){
				if(displayedElements.length > 2 && !elements) 	Styling.removeClass(this.page.upToDateMessage,	'hidden');
				else 											Styling.addClass(this.page.upToDateMessage, 	'hidden');
			}

			if(this.page.placeholder){
				Styling.removeClass(this.page.placeholder, 'loading');
				if(this.list.querySelector('.' + this.elementClass)) 	Styling.removeClass(this.page.placeholder, 	'active');
				else 													Styling.addClass(this.page.placeholder, 	'active');
			}

			this.checkScroll();

			return true;
		};
		LazyLoader.prototype.clearList = function(){
			this.list.innerHTML = '';
			this.clearStartIndex();
		};
		LazyLoader.prototype.clearStartIndex = function(){
			this.startIndex = null;
		};

	// CHECK SCROLL
		LazyLoader.prototype.checkScroll = function(){
			if(this.loading) return;
			if(!this.page.isVisible()) return;

			if(this.needsLoadMore()){
				var elements = this.list.querySelectorAll('.' + this.elementClass);
				var lastElement = (elements.length > 0) ? elements[elements.length - 1] : null;
				var startIndex = lastElement ? lastElement[this.page_index] : null;

				if(!startIndex || (this.startIndex && ((this.order === -1 && startIndex >= this.startIndex) || this.order === 1 && startIndex <= this.startIndex))) return;
				else this.startIndex = startIndex;

				this.getList(startIndex);
			}
		};
		LazyLoader.prototype.needsLoadMore = function(){
			if(this.scrollDirection === 'horizontal'){
				return this.scroller.scrollWidth - this.scroller.scrollLeft < this.screenEndRatio * this.scroller.clientWidth;
			}else{
				return this.scroller.scrollHeight - this.scroller.scrollTop < this.screenEndRatio * this.scroller.clientHeight;
			}
		};

	// LOCK SEARCH KEYBOARD
		var lockSearchKeyboard = function(e){
			setTimeout(function(){
				e.target.focus();
			}, 0);
			e.preventDefault();
			e.stopPropagation();
		};
		LazyLoader.prototype.lockSearch = function(){
			if(this.page.search){
				this.page.search.addEventListener('blur', lockSearchKeyboard, true);
				this.page.search.addEventListener('focusout', lockSearchKeyboard, true);
			}
		};
		LazyLoader.prototype.unlockSearch = function(){
			if(this.page.search){
				this.page.search.removeEventListener('blur', lockSearchKeyboard, true);
				this.page.search.removeEventListener('focusout', lockSearchKeyboard, true);
			}
		};

	return LazyLoader;
});
